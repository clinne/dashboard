# configure logging
import logging

logging.basicConfig(encoding="utf-8", level=logging.INFO)
logger = logging.getLogger(__name__)

from flask import Flask, jsonify, request
from db import create_connection, DATABASE
from update_db import update_uberspace
import json
import time

# Create the app
app = Flask(__name__)


@app.before_request
def information_logging():
    logger.info(f"entering route /{request.endpoint}")


@app.route("/uberspace", methods=["GET"])
def get_uberspace():

    conn = create_connection(DATABASE)
    logging.info("Connection created")
    cur = conn.cursor()
    cur.execute("SELECT * FROM uberspace")
    rows = cur.fetchall()
    keys = [
        "id",
        "guthaben",
        "wunschpreis",
        "domains_webserver",
        "domains_mailserver",
        "hostname",
        "ipv4",
        "username",
    ]
    data = [dict(zip(keys, row)) for row in rows]
    logging.info("Return data")
    return json.dumps(data)


@app.route("/refresh_uberspace", methods=["GET"])
def refresh_uberspace():
    start_time = time.time()
    logging.info(f"Start uberspace update {start_time}")
    update_uberspace()
    logging.info(f"Successful update in {time.time() - start_time}")
    return "True"


@app.route("/bank_chart", methods=["GET"])
def get_bank1822():
    conn = create_connection(DATABASE)
    logging.info("Connection created")
    cur = conn.cursor()
    cur.execute(
        "SELECT id, buchungstag, SUM(soll_haben), SUM(soll_haben) OVER (ROWS UNBOUNDED PRECEDING) FROM bank1822 WHERE COALESCE(buchungstag, soll_haben) IS NOT NULL GROUP BY buchungstag ORDER BY date(buchungstag)"
    )
    rows = cur.fetchall()
    keys = ["id", "buchungstag", "ist", "per"]
    data = [dict(zip(keys, row)) for row in rows]
    logging.info("Return data")
    return json.dumps(data)


@app.route("/bank_table", methods=["GET"])
def get_table_bank1822():
    conn = create_connection(DATABASE)
    logging.info("Connection created")
    cur = conn.cursor()
    cur.execute("SELECT * FROM bank1822 ORDER BY date(buchungstag)")
    rows = cur.fetchall()
    keys = [
        "id",
        "kontonummer",
        "datum_zeit",
        "buchungstag",
        "wertstellung",
        "soll_haben",
        "buchungsschluessel",
        "buchungsart",
        "empfaenger_auftraggeber_name",
        "empfaenger_auftraggeber_iban",
        "empfaenger_auftraggeber_bic",
        "glaeubiger_id",
        "mandatsreferenz",
        "mandatsdatum",
        "end_to_end_identifikation",
        "verwendungszweck",
    ]
    data = [dict(zip(keys, row)) for row in rows]
    logging.info("Return data")
    return json.dumps(data)


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
    # app.run()

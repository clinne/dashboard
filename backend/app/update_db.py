from sqlite3 import Error
from db import create_connection, DATABASE
from uberspace import update_data as uberspace_update
from bank import update_data as bank_update


def update_uberspace():
    # create a database connection
    conn = create_connection(DATABASE)
    with conn:
        conn.cursor().execute("DELETE FROM uberspace;")
        conn.commit()
        uberspace = uberspace_update()
        conn.executemany(
            "INSERT INTO uberspace (guthaben, wunschpreis, domains_webserver, domains_mailserver, hostname, ipv4, username) VALUES (:guthaben, :wunschpreis, :domains_webserver, :domains_mailserver, :hostname, :ipv4, :username);",
            uberspace,
        )


def update_1822direct():
    # create a database connection
    conn = create_connection(DATABASE)
    with conn:
        df = bank_update()
        conn.cursor().execute("DELETE FROM bank1822;")
        conn.commit()
        df.to_sql("bank1822", conn, if_exists="append", index=False)


def main():
    update_1822direct()
    # update_uberspace()


if __name__ == "__main__":
    main()

sql_create_statements = [
    """ CREATE TABLE IF NOT EXISTS uberspace (
                                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                                    guthaben text,
                                    wunschpreis text,
                                    domains_webserver text,
                                    domains_mailserver text,
                                    hostname text,
                                    ipv4 text,
                                    username text
                                ); """,
    """ CREATE TABLE IF NOT EXISTS bank1822 (
                                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                                    kontonummer text,
                                    datum_zeit text,
                                    buchungstag text,
                                    wertstellung text,
                                    soll_haben real,
                                    buchungsschluessel text,
                                    buchungsart text,
                                    empfaenger_auftraggeber_name text,
                                    empfaenger_auftraggeber_iban text,
                                    empfaenger_auftraggeber_bic text,
                                    glaeubiger_id text,
                                    mandatsreferenz text,
                                    mandatsdatum text,
                                    end_to_end_identifikation text,
                                    verwendungszweck text
                                ); """
]

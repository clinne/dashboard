import sqlite3
from sqlite3 import Error
from create_statements import sql_create_statements

DATABASE = r"dashboard.db"


def create_table(conn, create_table_sql):
    """create a table from the create_table_sql statement
    :param conn: Connection object
    :param create_table_sql: a CREATE TABLE statement
    :return:
    """
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)


def create_uberspace(conn, data):
    """
    Create a new uberspace info into the uberspace table
    :param conn:
    :param project:
    :return: project id
    """
    sql = """ INSERT INTO projects(name,begin_date,end_date)
              VALUES(?,?,?) """
    cur = conn.cursor()
    cur.execute(sql, data)
    conn.commit()
    return cur.lastrowid


def create_connection(db_file):
    """create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)

    return conn


if __name__ == "__main__":
    conn = create_connection(DATABASE)
    for statement in sql_create_statements:
        create_table(conn, statement)

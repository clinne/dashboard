import pandas as pd
from db import create_connection, DATABASE
import numpy as np
from utils import remove_umlaut

class Bank1822:
    def __init__(self):
        self.df = pd.DataFrame()

    def read_data(self, path="data/umsaetze-1235600015-26.06.2022_11 04.csv"):
        self.df = pd.read_csv(path, sep=";", encoding="unicode_escape", decimal=",")

    def transform_data(self):
        cols = [
            "Vwz.0",
            "Vwz.1",
            "Vwz.2",
            "Vwz.3",
            "Vwz.4",
            "Vwz.5",
            "Vwz.6",
            "Vwz.7",
            "Vwz.8",
            "Vwz.9",
            "Vwz.10",
            "Vwz.11",
            "Vwz.12",
            "Vwz.13",
            "Vwz.14",
            "Vwz.15",
            "Vwz.16",
            "Vwz.17",
        ]
        self.df["verwendungszweck"] = self.df[cols].apply(
            lambda row: "".join(row.fillna("").values.astype(str)), axis=1
        )
        self.df.drop(columns=cols, inplace=True)
        self.df["Soll/Haben"] = (
            self.df["Soll/Haben"]
            .str.replace(".", "", regex=True)
            .str.replace(",", ".", regex=True)
            .astype(float)
        )
        self.df.columns = self.df.columns.str.replace('/','_').str.replace(' ','_').str.replace('-','_').str.lower()
        self.df.columns = list(map(remove_umlaut, self.df.columns))
        self.df['buchungstag'] = pd.to_datetime(self.df['buchungstag'], format='%d.%m.%Y')

    def update_data_in_db(self):
        conn = create_connection(DATABASE)
        self.df.to_sql("bank", conn, if_exists="replace", index=False)

def update_data():
    bank = Bank1822()
    bank.read_data()
    bank.transform_data()
    # bank.update_data_in_db()
    return bank.df

def main():
    bank = Bank1822()
    bank.read_data()
    bank.transform_data()
    bank.update_data_in_db()


if __name__ == "__main__":
    main()

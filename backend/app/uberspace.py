from datetime import datetime
import requests
from bs4 import BeautifulSoup
import json
import os


class UberspaceWebRequest:
    def __init__(self, username: str, password: str):
        self.session = requests.Session()
        self.cookie_file = f"cookie_{datetime.now()}.txt"
        self.url = "https://dashboard.uberspace.de/login"
        self.username = username
        self.password = password
        self.data = {}

    def get_account_info(self):
        data = self._request_data()
        response = self.session.post(self.url, data=data)
        response = self.session.get(
            "https://uberspace.de/dashboard/accountinfo?format=json", data=data
        )
        return json.loads(response.content.decode("utf-8"))

    def _get_csrf_token(self):
        response = self.session.get(self.url)
        soup = BeautifulSoup(response.content, features="html.parser")
        csrf_token = soup.select_one("[name=_csrf_token]").get("value")
        return csrf_token

    def _request_data(self):
        csrf_token = self._get_csrf_token()
        data = {
            "_csrf_token": csrf_token,
            "login": self.username,
            "password": self.password,
            "submit": "login",
        }
        return data

    def get_data(self):
        data = self.get_account_info()
        self.data = {
            "guthaben": data["current_amount"],
            "wunschpreis": data["price"],
            "domains_webserver": json.dumps(data["domains"]["web"]),
            "domains_mailserver": json.dumps(data["domains"]["mail"]),
            "hostname": data["host"]["fqdn"],
            "ipv4": data["host"]["ipv4"],
            "username": data["login"],
        }


def update_data():
    infos = []
    username = (
        json.loads(os.getenv("UBERSPACE_USERNAME"))
        if os.getenv("UBERSPACE_USERNAME")
        else None
    )
    password = (
        json.loads(os.getenv("UBERSPACE_PASSWORD"))
        if os.getenv("UBERSPACE_PASSWORD")
        else None
    )

    for user, pw in zip(username, password):
        uberspace = UberspaceWebRequest(user, pw)
        uberspace.get_data()
        infos.append(uberspace.data)
    return infos

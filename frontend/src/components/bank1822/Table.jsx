import { DataGrid } from '@mui/x-data-grid'
import React, {useState, useEffect} from "react";

    
export default function Table() {
    const [data, setData] = useState()
    const [loading, setLoading] = useState(true);
  
    // useEffect(() => {
    //   fetch("/bank_table").then((response) => {
    //     response.json();
    // }
    //   ).then(
    //     (data) => {
    //       setData(data)
    //       setLoading(false);
    //     }
    //   ).catch(error => console.log(error))
    // });

    // const [resp, setGitData] = useState({ data: null, repos: null });

  

    if (loading) return <p>Loading</p>;
    const columns = [
    { field: 'kontonummer', headerName: 'kontonummer', flex: 1},
    { field: 'datum_zeit', headerName: 'datum_zeit', flex: 1},
    { field: 'buchungstag', headerName: 'buchungstag', flex: 1},
    { field: 'wertstellung', headerName: 'wertstellung', flex: 1},
    { field: 'soll_haben', headerName: 'soll_haben', flex: 1 },
    { field: 'buchungsschluessel', headerName: 'buchungsschluessel', flex: 1 },
    { field: 'buchungsart', headerName: 'buchungsart', flex: 1},
    { field: 'buchungsschluessel', headerName: 'buchungsschluessel', flex: 1 },
    { field: 'empfaenger_auftraggeber_name', headerName: 'empfaenger_auftraggeber_name', flex: 1 },
    { field: 'empfaenger_auftraggeber_iban', headerName: 'empfaenger_auftraggeber_iban', flex: 1 },
    { field: 'empfaenger_auftraggeber_bic', headerName: 'empfaenger_auftraggeber_bic', flex: 1 },
    { field: 'glaeubiger_id', headerName: 'glaeubiger_id', flex: 1 },
    { field: 'mandatsreferenz', headerName: 'mandatsreferenz', flex: 1 },
    { field: 'mandatsdatum', headerName: 'mandatsdatum', flex: 1 },
    { field: 'end_to_end_identifikation', headerName: 'end_to_end_identifikation', flex: 1 },
    { field: 'verwendungszweck', headerName: 'verwendungszweck', flex: 1 }
    ]
    return (
        <div className="table">
        <h3 className="tableTitle">Kosten</h3>
        <DataGrid className="dataTable2"
            // autoHeight {...data}
            rows={data}
            columns={columns}
            // pageSize={20}
            disableExtendRowFullWidth={true}
            // rowsPerPageOptions={[5]}
            checkboxSelection
        />
</div>
    )
}
import "./chart.css"
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip,  ResponsiveContainer } from 'recharts';
import React, {useState, useEffect} from "react";

export default function Chart({ grid }) {
  const [data, setData] = useState()
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    fetch("/bank_chart").then(
      response => response.json()
      // console.log(response.json())
    ).then(
      (data) => {
        setData(data)
        setLoading(false);
      }
    ).catch(error => console.log(error))
  }, []);

  return (
    <div className="chart">
        <h3 className="chartTitle">Kosten</h3>
        <ResponsiveContainer width="100%" aspect={4 / 1}>
        <LineChart data={data}>
            <XAxis dataKey={"buchungstag"} stroke="#5550bd"/>
            
            <YAxis />
            <Line type={"monotone"} dataKey="ist"stroke="#5550bd"/>
            <Line type={"monotone"} dataKey="per"stroke="#82ca9d"/>
            <Tooltip />
            {grid && <CartesianGrid stroke="#e0dfdf" strokeDasharray={"5 5"} />}
        </LineChart>
        </ResponsiveContainer>
    </div>
  )
}

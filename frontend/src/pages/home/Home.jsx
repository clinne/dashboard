import FeaturedInfo from "../../components/featuredInfo/FeaturedInfo"
import Chart from "../../components/charts/Chart"
import Table from "../../components/bank1822/Table"
import "./home.css"
import { DataGrid } from '@mui/x-data-grid'
import { Button } from '@mui/material'
import React, {useState, useEffect} from "react";
import axios from 'axios'

axios.defaults.baseURL = "http://localhost:5000"


export default function Home() {
  const [data, setData] = useState({ bank_data: null, uberspace_table: null })
  const [loading, setLoading] = useState(true);
  
  // const [Users, fetchUsers] = useState([])

  // const getData = () => {
  //   fetch('/bank_table')
  //     .then((res) => res.json())
  //     .then((res) => {
  //       console.log(res)
  //       fetchUsers(res)
  //     })
  // }

  // useEffect(() => {
  //   get_uberspace()
    // getData()
    // get_bank_table()
    // fetch("/uberspace").then(
    //   response => response.json()
    // ).then(
    //   (data) => {
    //     setData(data)
    //     setLoading(false);
    //   }
    // ).catch(error => console.log(error))
  // }, []);

  useEffect(() => {
    const fetchData = async () => {
      const [respBankTable, respUberspace] = await Promise.all([axios.get("/bank_table", {
        headers: {
          'Access-Control-Allow-Origin' : '*',
    }}), axios(`/refresh_uberspace`, {
      headers: {
        'Access-Control-Allow-Origin' : '*',
  }})]);
      setData({ data: respBankTable.data, uberspace_table: respUberspace.data });
    };

    fetchData();
  }, []);
  
  function refresh_uberspace() {
    fetch('/refresh_uberspace').then((response) => {
      if(!response.ok) throw new Error(response.status);
      else get_uberspace();
    })}

  function get_uberspace() {
      fetch('/uberspace').then(
        response => response.json()
      ).then(
        (data) => {
          setData(data)
          setLoading(false);
        }
      ).catch(error => console.log(error))}
  


  const columns = [
    { field: 'guthaben', headerName: 'Guthaben', flex: 1},
    { field: 'wunschpreis', headerName: 'Wunschpreis', flex: 1},
    { field: 'domains_webserver', headerName: 'Domains - Web', flex: 4},
    { field: 'domains_mailserver', headerName: 'Domains - Mail', flex: 3},
    { field: 'hostname', headerName: 'Hostname', flex: 2 },
    { field: 'ipv4', headerName: 'IPv4', flex: 1 },
    { field: 'username', headerName: 'User', flex: 1}
  ]
  if (loading) return <p>Loading</p>;
  return (
    <div className="home">
        <FeaturedInfo />
        <Chart />
        <Table />
        <div className="homeWidgets"></div>
        <DataGrid className="dataTable"
          autoHeight {...data}
          rows={data}
          columns={columns}
          pageSize={10}
          disableExtendRowFullWidth={true}
          rowsPerPageOptions={[5]}
          checkboxSelection
        />
        <Button onClick={refresh_uberspace}>Request new Data</Button>
        <Button onClick={get_uberspace}>Refresh</Button>

    </div>
  )
}
